$(document).ready(function(){
   var mails = ['kevin.wiegand@web.de'],
       users = [],
       user_mail,
       user_name,
       user_id,
       user_mail_cookie,
       user_id_cookie,
       user_name_cookie,
       email_regex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
       users = {
           ids:{
               '1553105820':{
                   name: 'Kevin',
                   mail: 'kevin.wiegand@web.de',
                   img: 'kevin.jpg'
               },
               '929594219':{
                    name: 'Nik',
                    mail: 'nikolaus@gradl.de ',
                   img: 'nik.jpg'
               },
               '3990883125':{
                   name: 'Alicia',
                   mail: 'encisoalicia@hotmail.com ',
                   img: 'alicia.jpg'
               },
               '3490883125':{
                   name: 'Andi',
                   mail: 'rossbach.privat@gmail.com ',
                   img: 'andi.jpg'
               }
           }
       };

    var dialog = $( "#user_mail_form" ).dialog({
        autoOpen: false,
        height: 300,
        width: 350,
        modal: true,
        buttons: {
            "Confirm Name and Email": add_name_and_mail,
            Cancel: function() {
                dialog.dialog( "close" );
            }
        }
    });


    function get_cookie(name) {
        var value = "; " + document.cookie,
            parts = value.split("; " + name + "=");
        if (parts.length === 2){ return parts.pop().split(";").shift();}
    }

    if(window.location.protocol === 'file:'){
        user_mail = 'alicia@alicia.de';
        user_id = '3990883125';
        user_name = 'Alicia';
        init_fire_pad();
    }else{
        user_mail_cookie = get_cookie('user_mail');
        user_id_cookie = get_cookie('user_id');
        user_name_cookie = get_cookie('user_name');
        if(user_id_cookie  === undefined){
            user_id = Math.floor(Math.random() * 9999999999).toString();
            document.cookie="user_id ="+ user_id;
        }else{
            user_id = user_id_cookie;
        }
        user_id = (user_id_cookie === undefined)? Math.floor(Math.random() * 9999999999).toString() : user_id_cookie;
        console.log('user_mail_cookie' + user_mail_cookie);
        console.log('user id cookie' + user_id);
        if(user_mail_cookie === undefined){
            dialog.dialog( "open" );
        }else{
            user_mail = user_mail_cookie;
            user_name = user_name_cookie;
            init_fire_pad();
        }

    }
    /*
    function add_user_pic_to_userlist(){
        var i;
        console.log(Object.keys(users['ids']).length);
        console.log(Object.keys(users['ids'])[0]);
        console.log();
        for(i=0; i<Object.keys(users['ids']).length; i++){
            console.log(Object.keys(users['ids'])[i]);
            console.log($('.firepad-user-'+Object.keys(users['ids'])[i]));
        }
    }
    add_user_pic_to_userlist();*/

    function add_name_and_mail(){
        user_mail = $('#user_mail').val();
        console.log(user_mail);
        user_name = $('#user_name').val();
        
        if( !( email_regex.test( user_mail ) ) ) {
            alert('Your mail is not valid. Please try again!');
        }else if(user_name.length === 0){
            alert('Please enter your name');
        }else{
            //email seems to be valid, save it in cookie
            document.cookie="user_mail ="+ user_mail;
            document.cookie="user_name ="+ user_name;
            dialog.dialog( "close" );
            init_fire_pad();
        }
    }




    function init_fire_pad(){
        var firepadRef = new Firebase('https://answerbot.firebaseio.com/firepads/1');
        //pruneHistory(firepadRef);
        var codeMirror = CodeMirror(document.getElementById('firepad'), { lineWrapping: true });
        var firepad = Firepad.fromCodeMirror(firepadRef, codeMirror,
            {
                richTextShortcuts: false,
                richTextToolbar: false,
                defaultText: '',
                userId: user_id
            });
        var firepadUserList = FirepadUserList.fromDiv(firepadRef.child('users'),
            document.getElementById('userlist'), user_id, user_name);
    }

    $('#countdown').countdown("2016/04/27 00:14:00", function(event) {
        $(this).text(
           event.strftime('%D days %H:%M')
        );
    });





});