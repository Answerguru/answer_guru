<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="resources/css/style.css">
    <link rel="stylesheet" href="resources/css/firepad-userlist.css">
    <!-- Firebase -->
    <script type="text/javascript" src="https://cdn.firebase.com/js/client/2.3.2/firebase.js"></script>
    <!--<script type="text/javascript" src="resources/js/fire_pad_history.js"></script>-->
    <!-- Firebase Userlist -->
    <script type="text/javascript" src="resources/js/firepad-userlist.js"></script>
    <link rel="stylesheet" href="resources/css/firepad-userlist.css">
    <!-- CodeMirror -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.10.0/codemirror.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.10.0/codemirror.css" />
    <!-- Firepad -->
    <link rel="stylesheet" href="https://cdn.firebase.com/libs/firepad/1.3.0/firepad.css" />
    <script src="https://cdn.firebase.com/libs/firepad/1.3.0/firepad.min.js"></script>
    <script type="text/javascript" src="resources/js/answer_guru.js"></script>
    <title>Answer God</title>
</head>
<body>
<div id="wrapper">
    <h1>Answer Guru</h1>

    <div id="firepad"></div>
    <div id="user_mail_form">
        <input type="text" id="user_name" placeholder="your name" />
        <input type="text" id="user_mail" placeholder="your email address" />
    </div>

</div>
</body>
</html>